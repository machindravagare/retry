package ai.wavelabs.demo;

import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IdCheck {
	@PostMapping(value = "/idscan/{id}")
	@Retryable(value = { NumberFormatException.class, NullPointerException.class }, maxAttempts = 3)
	public ResponseEntity<String> idscanner(@PathVariable("id") Integer id) {
		if (id == 11290) {
			return ResponseEntity.status(200).body("id correct");
		} else {
             return ResponseEntity.status(200).body("Id is not correct");
		}
	}
		@Recover
		public ResponseEntity<String> recoverMethod(NumberFormatException ex)
		{
			return ResponseEntity.status(200).body("please try again ");
		}
		@Recover
		
		public ResponseEntity<String> recover(NullPointerException ex)
		{
			return ResponseEntity.status(200).body("please try again once");
		}
		

	}
